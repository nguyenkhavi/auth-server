import authRoutes from "../modules/auth/authRoutes";
const useRoutes = (app) => {
    app.use("/v1/auth", authRoutes);
};

export default useRoutes;
