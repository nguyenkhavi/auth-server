import express from "express";
import * as controllers from "./authControllers";
const authRoutes = new express.Router();

authRoutes.get("/check", controllers.authJwt, controllers.check);
authRoutes.post("/refresh-token", controllers.refreshToken);
authRoutes.post("/signup", controllers.signUp);
authRoutes.post("/login", controllers.logIn);
authRoutes.post("/login/google", controllers.loginWithGoogle);
authRoutes.post("/login/facebook", controllers.loginWithFacebook);
authRoutes.post("/forgot-password", controllers.forgotPassword);

export default authRoutes;
