import mongoose from "mongoose";

const useDatabase = () => {
    mongoose.connect(process.env.MONGO_URI, {
        useUnifiedTopology: true,
        useNewUrlParser: true,
    });
    mongoose.connection.on("error", (err) => console.log(err));
    mongoose.connection
        .once("open", () => console.log("> MongoDB Running..."))
        .on("error", (e) => {
            throw e;
        });
};

export default useDatabase;
