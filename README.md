# NodeJS JWT Authentication sample with JWT

This is a NodeJS API that supports username and password authentication with JWTs. How awesome is that?

## Auth APIs

#### POST `/v1/auth/login`

You can do a POST to `/v1/auth/login` to log in.

The body must have:

-   `username`: The username
-   `password`: The password

It returns the following:

```json
{
  "accessToken": {jwt},
  "refreshToken": {jwt},
  "user":{userInfo}
}
```

The `accessToken` and `refreshToken` are signed with the secret located at the `.dev.env` and `.env` file.

#### POST `/v1/auth/signup`

You can do a POST to `/v1/auth/signup` to register.

The body must have:

-   `username`: The username
-   `password`: The password
-   `email`: The email.

It returns the following:

```json
{
  "accessToken": {jwt},
  "refreshToken": {jwt},
  "user":{userInfo}
}
```

#### GET `/v1/auth/check`

You can do a POST to `/v1/auth/check` to check the access token.

The header must have:

-   `Authorization`: Bearer token

It returns the following:

```json
{
  "accessToken": {jwt},
  "refreshToken": {jwt},
  "user":{userInfo}
}
```

#### POST `/v1/auth/refresh-token`

You can do a POST to `/v1/auth/refresh-token` to refresh the access token.

The body must have:

-   `refreshToken`: The refresh token,

It returns the following:

```json
{
  "accessToken": {jwt},
}
```

#### POST `/v1/auth/forgot-password`

You can do a POST to `/v1/auth/forgot-password` to reset user's password.

The body must have:

-   `email`: The email,

It will send new password via email provided

#### POST `/v1/auth/login/google`

You can do a POST to `/v1/auth/login/google` to log in with google.

The body must have:

-   `token`: The google token.

It returns the following:

```json
{
  "accessToken": {jwt},
  "refreshToken": {jwt},
  "user":{userInfo}
}
```

#### POST `/v1/auth/login/facebook`

You can do a POST to `/v1/auth/login/facebook` to log in with google.

The body must have:

-   `token`: The facebook token,

It returns the following:

```json
{
  "accessToken": {jwt},
  "refreshToken": {jwt},
  "user":{userInfo}
}
```
